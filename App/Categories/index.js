import { AsyncStorage } from 'react-native'

export default class Categories {
    static instance = Categories.instance || new Categories()

    allCategories = async() => {
        if(this.list) {
            // if(__DEV__) console.log("Cached Categories: ", this.list)
            return this.list
        }

        const localCategories = await retrieveLocalCategories()
        if(localCategories) {
            // if(__DEV__) console.log("Local Categories: ", localCategories)
            this.list = localCategories
            if(localCategories) return localCategories
        }

        const remoteCategories = await fetchRemoteCategories()
        if(remoteCategories) {
            // if(__DEV__) console.log("Remote Categories: ", remoteCategories)
            this.list = remoteCategories
            saveCategoriesLocally(remoteCategories)
        }
    }

}

export const search = ({ categories = [], text = '' }) => {
    const categoriesWithText = []
    categories.map(category => {
        if(category.categoryName.toLowerCase().includes(text.toLowerCase())) categoriesWithText.push(category)
        else {
            const subCategoriesWithText = []
            category.subcategories.map(subCategory => {
                if(subCategory.subCategoryname.toLowerCase().includes(text.toLowerCase())) subCategoriesWithText.push(subCategory)
                else {
                    const itemsWithText = []
                    subCategory.data.map(item => {
                        if(item.toLowerCase().includes(text.toLowerCase())) itemsWithText.push(item)
                    })
                    if(itemsWithText.length) subCategoriesWithText.push({ ...subCategory, data: itemsWithText })
                }
            })
            if(subCategoriesWithText.length) categoriesWithText.push({ ...category, subcategories: subCategoriesWithText })
        }
    })
    return categoriesWithText
}


// Network
fetchRemoteCategories = async() => {
    const parseCategoriesResponse = (response) => {
        if(response) {
            try {
                let parseCategoriesResponse = response.categories.map(obj => obj.category)
                let parseCategoriesResponseString = JSON.stringify(parseCategoriesResponse)
                parseCategoriesResponseString = parseCategoriesResponseString.replace(/"items"/g, '"data"') // converting items key to data, for rendering sectionlist
                return JSON.parse(parseCategoriesResponseString)
            } catch (error) {
                if(__DEV__) console.log('[Error] Parsing:\n', error)
            }
        }
    }
    try {
        const categoriesAPI = 'https://api.myjson.com/bins/mbtzw'
        let response = await fetch(categoriesAPI)
        let responseJson = await response.json()
        return parseCategoriesResponse(responseJson)
    } catch (error) {
        if(__DEV__) console.log('[Error] Fetching remote categories:\n', error)
    }
}

// AsyncStorage
const saveCategoriesLocally = async(categories) => {
    try {
        await AsyncStorage.setItem('categories', JSON.stringify(categories))
    } catch (error) {
        if(__DEV__) console.log('[Error] Saving categories:\n', error)
    }
}

const retrieveLocalCategories = async() => {
    try {
        const categories = await AsyncStorage.getItem('categories')
        if (categories) return JSON.parse(categories)
    } catch (error) {
        if(__DEV__) console.log('[Error] Retrieving categories:\n', error)
    }
}
