import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

export default ({  }) => (
    <View style={styles.container}>
        <Text style={styles.titleText}>
            OH CRUMBS!
        </Text>
        <Text style={styles.descriptionText}>
            {'No result found. Please check spelling or try\na different search.'}
        </Text>
    </View>
)

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleText: {
        fontSize: 16,
        color: 'grey',
        fontWeight: '500',
        textAlign: 'center',
    },
    descriptionText: {
        marginTop: 20,
        fontSize: 13,
        color: 'darkgrey',
        textAlign: 'center',
    }
})
