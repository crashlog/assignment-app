import React from 'react'
import { StyleSheet, View, Image, TextInput } from 'react-native'

import { assets, colors } from '../Res'

export default (props) => (
    <View style={styles.searchBarContainer}>
        <Image style={styles.searchIcon} source={assets.icons.search} resizeMode='contain' />
        <TextInput autoCorrect={false} style={styles.searchTextField} clearButtonMode='while-editing' maxLength={40} { ...props } />
    </View>
)

const styles = StyleSheet.create({
    searchBarContainer: {
        flexDirection: 'row',
        height: 50,
        width: '100%',
        backgroundColor: colors.searchBarBackground,
        paddingHorizontal: 15,
        alignItems: 'center',
        borderRadius: 5
    },
    searchIcon: {
        width: 20,
        height: 20,
        marginRight: 10,
        tintColor: '#adadad'
    },
    searchTextField: {
        flex: 1,
        fontSize: 17,
        fontWeight: '500',
        color: 'grey'
    }
})
