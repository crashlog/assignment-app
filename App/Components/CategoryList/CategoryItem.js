import React, { useState } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, ImageBackground } from 'react-native'
import Collapsible from 'react-native-collapsible'

import { assets, colors, styles as commonStyles } from '../../Res'
import { SubCategory } from '../../Components'

export default ({ item, index, onPress = () => null, isCategoryCollapsed = true, searchKeyword = '' }) => {

    const shouldRenderServings = item.servingSize && item.servingSize.length
    const colorCodeWithAlpha = `${item.colorCode}40`    // 40 = 25% opacity in hex
    const caretRotation = { transform: [{ rotate: isCategoryCollapsed ? '0deg' : '180deg' }] }
    const shouldRenderProtip = !isCategoryCollapsed

    return(
        <View>
            <View style={[styles.container, commonStyles.shadowStraight]}>
                <TouchableOpacity style={[styles.categoryContainer, commonStyles.shadowStraight]} onPress={() => onPress(item, index)} activeOpacity={0.5}>
                    <View style={[styles.leftIconContainer, { backgroundColor: colorCodeWithAlpha }]}>
                        <Image style={[styles.leftIcon, { tintColor: item.colorCode }]} source={assets.categories[item.localImagePath]} resizeMode='contain' />
                    </View>

                    <Text style={[styles.categoryName, { color: item.colorCode }]}>{item.categoryName}</Text>
                    {shouldRenderServings && <Text style={styles.categoryName}>({item.servingSize})</Text>}

                    <View style={{ flex: 1 }} />
                    <Image style={[styles.caret, caretRotation]} source={assets.icons.caret} resizeMode='contain' />
                </TouchableOpacity>
                <Collapsible collapsed={isCategoryCollapsed}>
                    <SubCategory subCategories={item.subcategories} categoryColor={item.colorCode} searchKeyword={searchKeyword} />
                    <Quote quote={item.quote} />
                </Collapsible>
            </View>
            {shouldRenderProtip && <ProTip tip={item.protip} />}
        </View>
    )
}

const Quote = ({ quote = '' }) => {
    if(quote.length)
        return(
            <View style={styles.quoteContainer}>
                <Text style={styles.quoteText}>{quote}</Text>
            </View>
        )
    return null
}

const ProTip = ({ tip = '' }) => {
    if(tip.length)
        return(
            <ImageBackground style={styles.protipContainer} source={assets.gradient}>
                <View style={styles.protipIconContainer}>
                    <Text style={[styles.protipText, { color: 'white', fontSize: 13 }]}>PRO TIP</Text>
                </View>
                <Text style={styles.protipText}>{tip}</Text>
            </ImageBackground>
        )
    return null
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 5,
        marginBottom: 15,
        backgroundColor: 'white'
    },
    categoryContainer: {
        padding: 5,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    leftIconContainer: {
        height: 50,
        marginRight: 10,
        aspectRatio: 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'lightgrey'
    },
    leftIcon: {
        width: 30,
        height: 30
    },
    categoryName: {
        fontSize: 17,
        fontWeight: '500',
        marginHorizontal: 3
    },
    caret: {
        width: 14,
        height: 14,
        marginRight: 10,
        tintColor: '#dbdbdb'
    },
    quoteContainer: {
        margin: 20,
        padding: 20,
        marginTop: 0,
        borderRadius: 10,
        backgroundColor: '#eef4f9',
    },
    quoteText: {
        color: 'grey',
        fontSize: 12,
        fontWeight: '500',
        textAlign: 'center',
    },
    protipContainer: {
        padding: 20,
        overflow: 'hidden',
        borderRadius: 10,
        marginBottom: 20,
        backgroundColor: '#eef4f9',
    },
    protipText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '500'
    },
    protipIconContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3f85ba',
        paddingHorizontal: 10,
        borderRadius: 12,
        marginBottom: 5,
        height: 24,
        width: 80
    }
})
