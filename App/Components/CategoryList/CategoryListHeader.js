import React from 'react'
import { View, Text } from 'react-native'
import { SearchBar } from '../'

import { styles } from '../../Res'

export default ({ title, placeholder, onChangeText }) => (
    <View style={{ paddingVertical: 10 }}>
        <Text style={styles.h1}>{title}</Text>
        <SearchBar placeholder='Try searching fat, sauces, etc...' onChangeText={onChangeText} />
    </View>
)
