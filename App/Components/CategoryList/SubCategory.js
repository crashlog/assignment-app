import React from 'react'
import { StyleSheet, View, Text, SectionList } from 'react-native'
import Highlighter from 'react-native-highlight-words'

import { styles as commonStyles } from '../../Res'

export default ({ subCategories, categoryColor, searchKeyword }) => {
    return(
        <SectionList sections={subCategories} keyExtractor={(item, index) => `subCategory_${index}`} style={styles.container}
            renderSectionHeader={({ section: { subCategoryname } }) => <SubCategoryHeader subCategoryName={subCategoryname} categoryColor={categoryColor} />}
            renderItem={({ item }) => <SectionItem item={item} highlightText={searchKeyword} />} ItemSeparatorComponent={UnderLine}
        />
    )
}

const SubCategoryHeader = ({ subCategoryName, categoryColor }) => {
    if(subCategoryName.length)
        return(
            <View style={styles.itemContainer}>
                <Text style={{ ...styles.sectionName, color: categoryColor }}>{subCategoryName}</Text>
            </View>
        )
    return null
}
const SectionItem = ({ item, highlightText }) => (
    <View style={styles.itemContainer}>
        <Highlighter highlightStyle={{ backgroundColor: 'yellow' }} searchWords={[highlightText]} textToHighlight={item}
        />
        {false && <Text style={styles.itemName}>{item}</Text>}
    </View>
)
const UnderLine = (props) => <View style={commonStyles.underLine} { ...props } />

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    itemContainer: {
        paddingVertical: 10,
        justifyContent: 'center'
    },
    itemName: {
        fontSize: 17,
        color: 'grey'
    },
    sectionName: {
        fontWeight: '400',
        fontSize: 20
    }
})
