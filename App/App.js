
import React, { Component } from 'react'
import { SafeAreaView, View, Text, TextInput, TouchableOpacity, FlatList } from 'react-native'
import { ListEmptyComponent, CategoryListHeader, CategoryItem } from './Components'
import { styles as commonStyles } from './Res'

import Categories, { search } from './Categories'

export default class FoodList extends Component {
    state = { searchText: '', categories: [], collapsedCategories: {} }

    render() {
        return (
            <SafeAreaView style={commonStyles.container}>
                <View style={commonStyles.container}>
                    <FlatList data={this.state.categories} keyExtractor={(item, index) => `category_${index}`} style={[commonStyles.container, { padding: 20 }]}
                        ListEmptyComponent={ListEmptyComponent} ListHeaderComponent={this.renderCategoriesListHeader} renderItem={this.renderCategoryItem}
                    />
                </View>
            </SafeAreaView>
        )
    }

    componentDidMount() {
        Categories.instance.allCategories().then((categories) => {
            this.categories = categories
            this.setState({ categories })
        })
    }

    renderCategoryItem = (itemProps) => <CategoryItem { ...itemProps } isCategoryCollapsed={this.state.collapsedCategories[`category_${itemProps.index}`]} onPress={this.handleCategoryOnPress} searchKeyword={this.state.searchText} />
    renderCategoriesListHeader = () => <CategoryListHeader title='Approved Foods List' onChangeText={this.handleSearchBarTextChange} searchText={this.state.searchText} />

    // Handlers
    handleSearchBarTextChange = (text = '') => {
        // Search after 300ms pause in typing.
        if(this.typingDelayTimeout) clearTimeout(this.typingDelayTimeout)
        this.setState({ searchText: text }, () => {
            const TYPING_DELAY = 250
            this.typingDelayTimeout = setTimeout(() => this.updateList(), TYPING_DELAY)
        })
    }

    handleCategoryOnPress = (item, index) => {
        let currentCollapsedState = this.state.collapsedCategories[`category_${index}`]
        if(currentCollapsedState == undefined) currentCollapsedState = true
        this.setState({ collapsedCategories: { ...this.state.collapsedCategories, [`category_${index}`]: !currentCollapsedState } })
    }

    // Internal
    updateList = () => {
        let categories;
        if(this.state.searchText.length) categories = search({ categories: this.categories, text: this.state.searchText })
        else categories = this.categories   // load all categories
        this.setState({ categories, collapsedCategories: {} }, () => {
            if(categories != this.categories) this.expandAllCategories()
        })  // update categories list, and expand if required
    }
    expandAllCategories = () => {
        if(this.state.categories != this.categories) {
            const collapsedCategories = {}
            for(let i=0; i < this.state.categories.length; i++) collapsedCategories[`category_${i}`] = false
            this.setState({ collapsedCategories })
        }
    }

}
