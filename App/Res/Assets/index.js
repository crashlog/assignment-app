export default {
    icons: {
        search: require('./Images/Icons/search.png'),
        caret: require('./Images/Icons/caret.png')
    },
    gradient: require('./Images/Gradient/gradient.png'),
    categories: {
        protein: require('./Images/Categories/protein.png'),
        seaFood: require('./Images/Categories/seaFood.png'),
        vegetable: require('./Images/Categories/vegetable.png'),
        fruits: require('./Images/Categories/fruits.png'),
        fats: require('./Images/Categories/fats.png'),
        sauces: require('./Images/Categories/sauces.png'),
    }
}
