
const colors = {
    shadowColor: '#d4d4d4',
    searchBarBackground: '#e7eff6',
    primaryBackgroundColor: '#e9e7ee'
}

const styles = {
    shadow: {
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 3,
        shadowOpacity: 0.5,
        elevation: 5
    },
    shadowStraight: {
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
        elevation: 3
    },
    shadowBottom: {
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 3,
        shadowOpacity: 0.5,
        elevation: 3
    },
    container: {
        flex: 1,
        backgroundColor: colors.primaryBackgroundColor
    },
    h1: {
        fontSize: 30,
        fontWeight: '600',
        paddingVertical: 20
    },
    underLine: {
        height: 1,
        backgroundColor: '#eff2f5'
    }
}

export { colors, styles }
